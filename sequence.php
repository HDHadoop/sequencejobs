<?php 

class Sequence {

	//Public variable for storing the completed processed sequence and return messages if there are any.
	public $completed_sequence = array();

	function __construct() {
   	}

   	function process_sequence($sequence) {
   		//Check the the data is fine 
   		if($this->check_sequence($sequence)) {
   			return;
   		}

   		//Keep track of the index where we break out of loop
   		$lastIndex; 
   		//Error var used to detect if we are in a deadlock.
   		$errors = false;

   		foreach($sequence as $key => $value) {
   			//If there is no value so dependency, process the job.
   			if (empty($value)) {
				$lastIndex = $key;
				$this->completed_sequence[] = $key;
				break;
			} 
			//Else if the job's dependency is in the completed sequence, process the job.
			else if(in_array($value, $this->completed_sequence)) {
				$lastIndex = $key;
				$this->completed_sequence[] = $key;
				break;
			} 
			//If we've reached the end of the array and couldn't process anything in jobs, then we must have reached a deadlock.
			else if(end($sequence) == $value) {
				$errors = true;
				break;
			}
   		}

   		//If there is a deadlock, set the return message
   		if($errors) {
   			$this->completed_sequence['returnMessage'] = 'Jobs can\'t have circular dependencies';
   		}

   		//Unset the index from jobs.
   		if(isset($lastIndex)) {
			unset($sequence[$lastIndex]);
		}

		//If jobs aren't empty and we are not in a deadlock, process again with the new list of jobs.
		if(!empty($sequence) && !$errors) {
			$this->process_sequence($sequence);
		} 


   	}

   	function check_sequence($sequence) {
   		//Errors flag used to detect if there was an error.
   		$errors = false;
   		foreach($sequence as $key => $value) {
   			//If the ke is empty, then set the return message.
   			if(empty($key)){
   				$errors = true;
				$this->completed_sequence['returnMessage'] =  'Jobs can\'t be empty';
				break;
			} 
			//Else if the job has a dependency on itself, set the return message.
			else if($key === $value) {
   				$errors = true;
   				$this->completed_sequence['returnMessage'] =  'Jobs can\'t be dependent on themselves';
   				break;
   			}
   			//Else if the job or value isn't a single alphabet character, set the return message.
   			else if(!preg_match("/^[a-zA-Z]$/", $key) && !preg_match("/^[a-zA-Z]$/", $value)) {
   				$errors = true;
				$this->completed_sequence['returnMessage'] = 'Jobs should be a single alphabet character, e.g. a b c';
				break;
			}
   		}

   		//Return errors, true or false.
   		return $errors;
   	}
}