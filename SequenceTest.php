<?php
use PHPUnit\Framework\TestCase;
require_once (dirname(__FILE__) . "/sequence.php");

class SequenceTest extends TestCase
{
    private $sequence;

    public function setUp() {
        $this->sequence = new Sequence();
    }
    // Test that when an empty job sequence is passed, an empty array is returned.
    public function testEmptySequencePassed()
    {
        $this->sequence->process_sequence(array());
        $this->assertEquals(array(), $this->sequence->completed_sequence);
    }

    //Test that when a single job with no dependency is passed through, it is returned.
    public function testSequenceReturnsSingleJob()
    {
        $this->sequence->process_sequence(array('a' => null));
        $this->assertEquals(array('a'), $this->sequence->completed_sequence);
    }

    // // Test that when several jobs with no dependencies passed through are simply returned.
    public function testSequenceJobsWithNoDependency()
    {
        $this->sequence->process_sequence(array('a' => null, 'b' => null, 'c' => null));
        $this->assertEquals(array('a','b','c'), $this->sequence->completed_sequence);
    }

    // Test that when there is a dependency, it is processed correctly with the dependency occuring before the job that is dependent on it.
    public function testSequenceJobWithDependency()
    {
        $this->sequence->process_sequence(array('a' => null, 'b' => 'c', 'c' => null));
        $this->assertEquals(array('a', 'c', 'b'), $this->sequence->completed_sequence);
    }

    // Test that when there is more than one job with a dependency, it is processed correctly.
    public function testSequenceJobsWithDependency()
    {
        $this->sequence->process_sequence(array('a' => null, 'b' => 'c', 'c' => 'f', 'd' => 'a', 'e' => 'b', 'f' => null));
        $this->assertEquals(array('a', 'd', 'f', 'c', 'b', 'e'), $this->sequence->completed_sequence);
    }

    // Test that when there is a job that is dependent on itself, it is picked up.
    public function testJobDependentOnSelf()
    {
        $this->sequence->process_sequence(array('a' => null, 'b' => null, 'c' => 'c'));
        $this->assertEquals('Jobs can\'t be dependent on themselves', $this->sequence->completed_sequence['returnMessage']);
    }

    // Test that when there is a circular dependency, it is picked up.
    public function testJobsWithCircularDependencies()
    {
        $this->sequence->process_sequence(array('a' => null, 'b' => 'c', 'c' => 'f', 'f' => 'b'));
        $this->assertEquals('Jobs can\'t have circular dependencies', $this->sequence->completed_sequence['returnMessage']);
    }

    // Test that when an empty Job is passed through, it is picked up.
    public function testEmptyJob()
    {
        $this->sequence->process_sequence(array(null => null));
        $this->assertEquals('Jobs can\'t be empty', $this->sequence->completed_sequence['returnMessage']);
    }

    // Test that jobs are a single alphabet character.
    public function testSingleAlphabetCharacter()
    {
        $this->sequence->process_sequence(array('1' => null));
        $this->assertEquals('Jobs should be a single alphabet character, e.g. a b c', $this->sequence->completed_sequence['returnMessage']);

        $this->sequence->process_sequence(array('aa' => null));
        $this->assertEquals('Jobs should be a single alphabet character, e.g. a b c', $this->sequence->completed_sequence['returnMessage']);
    }
}
